Source: epiphany-browser
Section: gnome
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: @GNOME_TEAM@
Build-Depends: appstream-util,
               at-spi2-core <!nocheck>,
               dbus <!nocheck>,
               debhelper-compat (= 13),
               desktop-file-utils,
               dh-sequence-gnome,
               gobject-introspection (>= 0.9.12-4~),
               gsettings-desktop-schemas-dev,
               gtk-doc-tools (>= 1.0),
               iso-codes (>= 0.35),
               itstool,
               libadwaita-1-dev (>= 1.3.1),
               libarchive-dev,
               libgcr-4-dev,
               libgdk-pixbuf-2.0-dev (>= 2.36.5),
               libgirepository1.0-dev (>= 0.10.7-1~),
               libglib2.0-dev (>= 2.67.4),
               libgstreamer1.0-dev,
               libgtk-4-dev (>= 4.9.3),
               libjson-glib-dev (>= 1.6),
               libportal-gtk4-dev (>= 0.6),
               libsecret-1-dev (>= 0.19),
               libsoup-3.0-dev (>= 2.99.4),
               libsqlite3-dev,
               libwebkitgtk-6.0-dev (>= 2.39.91),
               libxml-parser-perl,
               libxml2-dev (>= 2.6.12),
               libxslt1-dev (>= 1.1.7),
               meson (>= 0.59.0),
               nettle-dev (>= 3.4),
               pkg-config,
               xauth <!nocheck>,
               xvfb <!nocheck>
Rules-Requires-Root: no
Build-Depends-Indep: libglib2.0-doc, libgtk-3-doc
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/gnome-team/epiphany-browser
Vcs-Git: https://salsa.debian.org/gnome-team/epiphany-browser.git
Homepage: https://wiki.gnome.org/Apps/Web

Package: epiphany-browser
Architecture: any
Depends: default-dbus-session-bus | dbus-session-bus,
         epiphany-browser-data (>= ${source:Version}),
         gsettings-desktop-schemas,
         iso-codes,
         ${misc:Depends},
         ${shlibs:Depends}
Conflicts: swfdec-mozilla
Recommends: ca-certificates, evince, yelp
Provides: gnome-www-browser, www-browser
Description: Intuitive GNOME web browser
 Epiphany is a simple yet powerful GNOME web browser targeted at
 non-technical users. Its principles are simplicity and standards
 compliance.
 .
 Simplicity is achieved by a well designed user interface and reliance
 on external applications for performing external tasks (such as reading
 email). Simplicity does not mean less features; Epiphany has everything
 a modern web browser is expected to have.
 .
 Standards compliance is achieved on the HTML side by using the
 WebKitGTK+ rendering engine (which is based on the engine used by
 Apple Safari and Google Chrome); and on the user interface side by
 closely following the GNOME Human Interface Guidelines (HIG) and by
 close integration with the GNOME desktop.

Package: epiphany-browser-data
Architecture: all
Depends: ${misc:Depends}
Recommends: epiphany-browser
Description: Data files for the GNOME web browser
 Epiphany is a simple yet powerful GNOME web browser targeted at
 non-technical users. Its principles are simplicity and standards
 compliance.
 .
 This package contains the common files, artwork and translations for
 Epiphany.
